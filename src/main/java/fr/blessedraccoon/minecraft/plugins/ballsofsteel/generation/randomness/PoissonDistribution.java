package fr.blessedraccoon.minecraft.plugins.ballsofsteel.generation.randomness;

public class PoissonDistribution extends BornableDistribution {

    private double lambda;

    public PoissonDistribution(int low, int high, double lambda) {
        super(low, high);
        this.lambda = lambda;
    }

    @Override
    public int getRandom() {
        double L = Math.exp(- this.lambda);
        double p = 1.0;
        int k = 0;

        do {
            k++;
            p *= Math.random();
        } while (p > L);

        return k - 1;
    }
    
}
