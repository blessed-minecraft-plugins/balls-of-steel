package fr.blessedraccoon.minecraft.plugins.ballsofsteel.commands;

import org.bukkit.entity.Player;

import fr.blessedraccoon.minecraft.plugins.ballsofsteel.gameplay.BOSStart;
import fr.blessedraccoon.minecraft.plugins.commandmanager.CommandPart;

public class StartCommand extends CommandPart {

    private BOSStart start;

    public StartCommand(BOSStart start) {
        this.start = start;
    }

    @Override
    public int getDeepLevel() {
        return 1;
    }

    @Override
    public String getDescription() {
        return "Starts the game";
    }

    @Override
    public String getName() {
        return "start";
    }

    @Override
    public String getSyntax() {
        return "/bos start";
    }

    @Override
    public boolean perform(Player player, String[] args) {
        start.start();
        return true;
    }

	@Override
	public String getPermission() {
		return "bos.start";
	}
    
}
