package fr.blessedraccoon.minecraft.plugins.ballsofsteel.generation.terrain;

import fr.blessedraccoon.minecraft.plugins.ballsofsteel.BOSInit;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.gameplay.scoreboard.StockDiamonds;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.TextColor;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class DiamondSign implements Listener {

    private static final Component FIRST_LINE = Component.text("[Stock]").color(TextColor.color(85, 255, 255));
    private static final Component SECOND_LINE = Component.text("Diamants").color(TextColor.color(85, 255, 85));


    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        int sum = 0;
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (e.getClickedBlock().getState() instanceof Sign) {
                Sign sign = (Sign) e.getClickedBlock().getState();
                if (sign.line(1).equals(FIRST_LINE) && sign.line(2).equals(SECOND_LINE)) {
                    Player player = e.getPlayer();
                    Inventory inv = player.getInventory();
                    ItemStack[] items = inv.getContents();
                    for(ItemStack item: items) {
                        if (item != null && item.getType() == Material.DIAMOND) {
                            sum += item.getAmount();
                        }
                    }
                    for(ItemStack item: items) {
                        if (item != null && item.getType() == Material.DIAMOND) {
                            inv.remove(item);
                        }
                    }
                    StockDiamonds.stock(e.getPlayer(), sum);
                }
            }
        }
    }

    public static void place(int x, int y, int z) {
        var block = BOSInit.getOverworld().getBlockAt(x, y, z);
        block.setType(Material.OAK_SIGN);
        var sign = (Sign) block.getState();
        sign.line(1, FIRST_LINE);
        sign.line(2, SECOND_LINE);
        sign.update();
    }

    @EventHandler
    public void unbreakableSign(BlockBreakEvent e) {
        // e.setCancelled(true);
    }

}
