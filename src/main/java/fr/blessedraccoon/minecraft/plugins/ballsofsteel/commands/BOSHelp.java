package fr.blessedraccoon.minecraft.plugins.ballsofsteel.commands;

import fr.blessedraccoon.minecraft.plugins.commandmanager.CommandManager;
import fr.blessedraccoon.minecraft.plugins.commandmanager.HelpCommandPattern;

public class BOSHelp extends HelpCommandPattern {

    public BOSHelp(CommandManager manager) {
        super(manager);
    }

	@Override
	public String getPermission() {
		return "bos.help";
	}
    
}
