package fr.blessedraccoon.minecraft.plugins.ballsofsteel.generation.randomness;

public abstract class BornableDistribution implements Distribution {

    protected int low, high;

    public BornableDistribution(int low, int high) {
        if (low < 0) low = 0;
        if (high < low) high = low + 1;
        this.low = low;
        this.high = high;
    }

    public int getBornedRandom() {
        int res = 0;
        do {
            res = this.getRandom();
        }
        while (res < this.low || res > this.high);
        return res;
            
    }

    public static BornableDistribution getDistributionByConfig(int low, int high, String distribution, double lambda) {
        BornableDistribution dist = null;
        switch (distribution) {
            case "PoissonDistribution":
                dist = new PoissonDistribution(low, high, lambda);
                break;
            case "UniformDistribution":
                dist = new UniformDistribution(low, high);
                break;
        }
        return dist;
    }

    public void setHigh(int high) {
        this.high = high;
    }

    public void setLow(int low) {
        this.low = low;
    }

    public int getHigh() {
        return high;
    }

    public int getLow() {
        return low;
    }
    
}
