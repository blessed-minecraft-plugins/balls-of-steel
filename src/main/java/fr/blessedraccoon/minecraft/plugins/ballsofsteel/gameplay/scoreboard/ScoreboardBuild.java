package fr.blessedraccoon.minecraft.plugins.ballsofsteel.gameplay.scoreboard;

import fr.blessedraccoon.minecraft.plugins.ballsofsteel.gameplay.teams.TeamManager;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.generation.terrain.TeamBall;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.TextColor;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import java.util.HashMap;
import java.util.Map;

public class ScoreboardBuild {

    public static final String NAME = "Balls of Steel";
    private static ScoreboardBuild instance;

    private int uniqueBlankLineID = 0;

    private Scoreboard sc;
    private Objective objective;
    private Map<Integer, Score> scores;

    private ScoreboardBuild() {
        this.sc = Bukkit.getScoreboardManager().getMainScoreboard();
        this.objective = sc.registerNewObjective(NAME, "dummy", Component.text(NAME).color(TextColor.color(164, 21, 242)));
        this.scores = new HashMap<>();
    }

    public void display() {
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
    }

    public void build() {
        this.blankLine(10);
        this.updateOnlinePlayersCount();
        this.line(ChatColor.GOLD + "Kills : 0", 8); // TODO Pas 0, à compléter
        this.blankLine(7);
        this.line(" ---------------- ", 6);
        this.blankLine(5);

        for(var team: TeamManager.TEAMS) {
            updateTeamScore(team);
        }

        this.blankLine(0);
        this.line(ChatColor.UNDERLINE + "play.ratoncraft.fr", -1);
    }

    public void blankLine(int index) {
        StringBuilder sb = new StringBuilder("");
        for (int i = 0; i < uniqueBlankLineID; i++) {
            sb.append(" ");
        }
        line(sb.toString(), index);
        this.uniqueBlankLineID++;
    }

    public void line(String content, int index) {
        if (this.scores.containsKey(index)) {
            sc.resetScores(this.scores.get(index).getEntry());
        }
        Score line = objective.getScore(content);
        line.setScore(index);
        this.scores.put(index, line);
    }

    public static synchronized ScoreboardBuild getInstance() {
        if (instance == null) {
            instance = new ScoreboardBuild();
        }
        return instance;
    }

    public void updateTeamScore(TeamBall team) {
        this.line(team.getColor() + team.getDisplayName() + ChatColor.RESET + " - " + ChatColor.GOLD + team.getScore(), 4 - team.getIndex());
    }

    public void updateOnlinePlayersCount() {
        this.line(ChatColor.DARK_AQUA + "" + Bukkit.getServer().getOnlinePlayers().size() + " Joueurs", 9);
    }





}
