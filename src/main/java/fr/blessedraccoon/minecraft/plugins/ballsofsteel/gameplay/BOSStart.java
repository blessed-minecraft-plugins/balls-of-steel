package fr.blessedraccoon.minecraft.plugins.ballsofsteel.gameplay;

import com.sk89q.worldguard.protection.flags.Flags;
import com.sk89q.worldguard.protection.flags.StateFlag;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.BOSInit;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.config.BOSConfig;
import fr.blessedraccoon.minecraft.plugins.timer.timing.TimeOutOfBoundsException;
import fr.blessedraccoon.minecraft.plugins.timer.timing.TimerManager;
import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.GameMode;
import org.bukkit.GameRule;
import org.bukkit.scoreboard.Team;

import fr.blessedraccoon.minecraft.plugins.ballsofsteel.gameplay.scoreboard.Health;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.gameplay.scoreboard.StockDiamonds;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.gameplay.teams.TeamManager;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.generation.MapGeneration;

public class BOSStart {
    
    private MapGeneration map;
    private StockDiamonds stockDiamonds;

    private static GameState state = GameState.WAITING_FOR_GENERATION;
    public enum GameState {
        WAITING_FOR_GENERATION,
        WAITING_TO_START,
        RUNNING,
        AFTER_END
    }

    public BOSStart(MapGeneration map) {
        this.map = map;
        state = GameState.WAITING_FOR_GENERATION;
    }

    public void start() {
        final TeamManager teams = map.getTeamManager();
        teams.setSpawns();
        this.stockDiamonds = new StockDiamonds();
        new Health();

        var timer = TimerManager.getInstance().getTimer("BOS");
        try {
            timer.addTime(BOSConfig.GAME_END_SECONDS.getValue());
        } catch (TimeOutOfBoundsException e) {
            Bukkit.getLogger().severe("The specified time for the BOS game is incorrect. Starting impossible");
            return;
        }

        TimerManager.getInstance().setVisible(timer);
        timer.setRunning(true);

        for (Team team : Bukkit.getScoreboardManager().getMainScoreboard().getTeams()) {
            team.setAllowFriendlyFire(false);
        }

        var center = map.getCenter().getRegion();
        center.setFlag(Flags.EXIT, StateFlag.State.ALLOW);
        center.setFlag(Flags.FEED_DELAY, 0);
        center.setFlag(Flags.FEED_AMOUNT, null);
        center.setFlag(Flags.PVP, StateFlag.State.ALLOW);


        var overworld = BOSInit.getOverworld();
        overworld.setDifficulty(Difficulty.HARD);
        overworld.setTime(0);
        overworld.setGameRule(GameRule.DO_MOB_SPAWNING, false);
        for(var p : Bukkit.getServer().getOnlinePlayers()) {
            p.getInventory().clear();
            p.setGameMode(GameMode.SURVIVAL);
            p.setHealth(0.0);
        }

        state = GameState.RUNNING;
    }

    public StockDiamonds getStockDiamonds() {
        return stockDiamonds;
    }

    public static GameState getGameState() {
        return state;
    }

    public static void setGameState(GameState state) {
        BOSStart.state = state;
    }

}
