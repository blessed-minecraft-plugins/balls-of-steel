package fr.blessedraccoon.minecraft.plugins.ballsofsteel.gameplay.scoreboard;

import fr.blessedraccoon.minecraft.plugins.ballsofsteel.gameplay.BOSStart;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class ScoreboardUpdater implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        this.updatePlayerCount();
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent e) {
        this.updatePlayerCount();
    }

    public void updatePlayerCount() {
        if (BOSStart.GameState.WAITING_TO_START == BOSStart.getGameState()) {
            ScoreboardBuild.getInstance().updateOnlinePlayersCount();
        }
    }

}
