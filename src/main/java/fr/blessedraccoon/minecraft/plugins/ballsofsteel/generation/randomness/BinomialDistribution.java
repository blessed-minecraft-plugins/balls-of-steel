package fr.blessedraccoon.minecraft.plugins.ballsofsteel.generation.randomness;

public class BinomialDistribution extends BornableDistribution {

    private double p;
    private int n;

    public BinomialDistribution(int low, int high, int n, double p) {
        super(low, high);
        this.n = Math.max(0, n);
        this.p = (p >= 0 && p <= 1) ? p : 0.5;
    }

    @Override
    public int getRandom() {
        int x = 0;
        BernouilliOrdeal bernouilli = new BernouilliOrdeal(p);
        for (int i = 0; i < this.n; i++) {
            x += (bernouilli.run()) ? 1 : 0;
        }
        return x;
    }
    
}
