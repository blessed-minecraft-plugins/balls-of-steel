package fr.blessedraccoon.minecraft.plugins.ballsofsteel.generation.terrain;

import fr.blessedraccoon.minecraft.plugins.ballsofsteel.gameplay.scoreboard.ScoreboardBuild;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.gameplay.teams.TeamManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.scoreboard.Team;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class TeamBall {

    private List<Material> blockVariants;
    private String displayName;

    private int score;

    private int x;
    private int y;
    private int z;
    private ChatColor color;

    private Protection protection;

    private int direction;

    public TeamBall(List<Material> variants, int x, int y, int z, int radius, int minDistance, String displayName, ChatColor color, int direction) {
        this.direction = direction;
        this.blockVariants = variants;
        this.x = (x < 0) ? x + radius + minDistance : x - radius - minDistance;
        this.y = y;
        this.z = (z < 0) ? z + radius + minDistance : z - radius - minDistance;
        this.displayName = displayName;
        this.color = color;
        this.score = 0;
    }

    @Nullable
    public Team getTeam() {
        var teams = Bukkit.getServer().getScoreboardManager().getMainScoreboard().getTeams();
        for (var team : teams) {
            if (team.getName() == this.getDisplayName()) {
                return team;
            }
        }
        return null;
    }

    public int getX() {
        return x;
    }

    public String getColorName() {
        return color.name();
    }

    public ChatColor getColor() {
        return color;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    public Material getMaterialVariant(int index) {
        return this.blockVariants.get(index % blockVariants.size());
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setScore(int score) {
        this.score = score;
        ScoreboardBuild.getInstance().updateTeamScore(this);
    }

    public void addScore(int score) {
        this.setScore(this.getScore() + score);
    }

    public int getIndex() {
        int i = 0;
        for(var team : TeamManager.TEAMS) {
            if (team == this) {
                return i;
            }
            i++;
        }
        return -1;
    }

    public int getDirection() {
        return direction;
    }

    public int getScore() {
        return this.score;
    }

    public Protection getProtection() {
        return protection;
    }

    public void setProtection(Protection protection) {
        this.protection = protection;
    }
}
