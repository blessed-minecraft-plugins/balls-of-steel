package fr.blessedraccoon.minecraft.plugins.ballsofsteel.config;

import fr.blessedraccoon.minecraft.plugins.ballsofsteel.BOSInit;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.generation.terrain.CoreMaterial;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.generation.terrain.TeamBall;
import fr.blessedraccoon.minecraft.plugins.configmanager.Config;
import fr.blessedraccoon.minecraft.plugins.configmanager.ConfigVariable;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;

public class BOSConfig extends Config {

    private static final Plugin plugin = Bukkit.getPluginManager().getPlugin(BOSInit.NAME);

    public static final ConfigVariable<String> OVERWORLD = new ConfigVariable<>(plugin, String.class, "overworld", "world");
    public static final ConfigVariable<Integer> MAP_RADIUS = new ConfigVariable<>(plugin, Integer.class, "generation.map.radius", 150);
    public static final ConfigVariable<Integer> MAP_CENTER_X = new ConfigVariable<>(plugin, Integer.class, "generation.map.center.x", 0);
    public static final ConfigVariable<Integer> MAP_CENTER_Z = new ConfigVariable<>(plugin, Integer.class, "generation.map.center.z", 0);

    public static final ConfigVariable<String> MIDDLE_ISLAND_SCHEM_PATH = new ConfigVariable<>(plugin, String.class, "generation.map.middle.path", "middle.schem");
    public static final ConfigVariable<Integer> MIDDLE_ISLAND_RADIUS = new ConfigVariable<>(plugin, Integer.class, "generation.map.middle.radius", 20);
    public static final ConfigVariable<String> TEAM_ISLAND_SCHEM_PATH = new ConfigVariable<>(plugin, String.class, "generation.map.teamBall.path", "team.schem");
    public static final ConfigVariable<Integer> TEAM_ISLAND_RADIUS = new ConfigVariable<>(plugin, Integer.class, "generation.map.teamBall.radius", 15);

    public static final ConfigVariable<Integer> MAX_BALL_NUMBER = new ConfigVariable<>(plugin, Integer.class, "generation.map.balls.number", 250);

    public static final ConfigVariable<Integer> MIN_DISTANCE_BETWEEN_BALLS_AND_BORDER = new ConfigVariable<>(plugin, Integer.class, "generation.map.balls.border.minDistanceBetween", 10);
    public static final ConfigVariable<Integer> MIN_DISTANCE_BETWEEN_BALLS = new ConfigVariable<>(plugin, Integer.class, "generation.map.balls.minDistanceBetween", 12);

    public static final ConfigVariable<Integer> MIN_BALL_RADIUS = new ConfigVariable<>(plugin, Integer.class, "generation.map.balls.radius.min", 4);
    public static final ConfigVariable<Integer> MAX_BALL_RADIUS = new ConfigVariable<>(plugin, Integer.class, "generation.map.balls.radius.max", 16);
    public static final ConfigVariable<Integer> LAMBDA_BALL_RADIUS = new ConfigVariable<>(plugin, Integer.class, "generation.map.balls.radius.lambda", 10);
    public static final ConfigVariable<String> BALL_RADIUS_DISTRIBUTION = new ConfigVariable<>(plugin, String.class, "generation.map.balls.radius.distribution", "PoissonDistribution");

    public static final ConfigVariable<Integer> MIN_DIAMONDS_PROPORTION = new ConfigVariable<>(plugin, Integer.class, "generation.map.balls.diamonds.min", 2);
    public static final ConfigVariable<Integer> MAX_DIAMONDS_PROPORTION = new ConfigVariable<>(plugin, Integer.class, "generation.map.balls.diamonds.max", 12);
    public static final ConfigVariable<Integer> LAMBDA_DIAMONDS_PROPORTION = new ConfigVariable<>(plugin, Integer.class, "generation.map.balls.diamonds.lambda", 7);
    public static final ConfigVariable<String> DIAMONDS_PROPORTION_DISTRIBUTION = new ConfigVariable<>(plugin, String.class, "generation.map.balls.diamonds.distribution", "PoissonDistribution");

    public static final ConfigVariable<Double> BALLS_SAME_CRUST_CHANCE = new ConfigVariable<>(plugin, Double.class, "generation.map.balls.materials.sameCrustChance", 0.85);

    public static final ConfigVariable<Integer> GAME_END_SECONDS = new ConfigVariable<>(plugin, Integer.class, "gameplay.end.value", 3600);
    public static final ConfigVariable<Integer> GAME_END_COUNTDOWN = new ConfigVariable<>(plugin, Integer.class, "gameplay.end.countdown", 10);

    public static final ConfigVariable<Integer> GAME_MIN_PLAYERS = new ConfigVariable<>(plugin, Integer.class, "gameplay.players.min", 12);
    public static final ConfigVariable<Integer> GAME_MAX_PLAYERS = new ConfigVariable<>(plugin, Integer.class, "gameplay.players.max", 16);

    public static final List<CoreMaterial> CORE_MATERIALS = readCores();

    public static final List<TeamBall> TEAMS = readTeams();

    private static List<TeamBall> readTeams() {
        String[] names = {"Vert", "Rouge", "Jaune", "Bleu"};
        String[] colors = {"GREEN", "RED", "YELLOW", "BLUE"};
        String[][] materials = {
                {"GREEN_WOOL", "LIME_WOOL"},
                {"RED_WOOL", "MAGENTA_WOOL"},
                {"YELLOW_WOOL", "ORANGE_WOOL"},
                {"BLUE_WOOL", "LIGHT_BLUE_WOOL"},
        };
        int[] directions = {180, 270, 0, 90};
        for(int i = 0 ; i < names.length ; i++) {
            final String path = "gameplay.teams.list." + i + ".";
            BOSInit.CONFIG.addDefault(path + "name", names[i]);
            BOSInit.CONFIG.addDefault(path + "color", colors[i]);
            BOSInit.CONFIG.addDefault(path + "direction", directions[i]);
            BOSInit.CONFIG.addDefault(path + "materials", materials[i]);
            BOSInit.CONFIG.addDefault(path + "activated", true);
        }

        List<TeamBall> teamBalls = new ArrayList<>();
        var section = BOSInit.CONFIG.getConfigurationSection("gameplay.teams.list");
        int[][] signs = {{1, 1}, {1, -1}, {-1, -1}, {-1, 1}};
        int i = 0;
        for(var team : section.getKeys(false)) {
            var activated = section.getBoolean(team + ".activated");
            if (activated) {
                var name = section.getString(team + ".name");
                var color = section.getString(team + ".color");
                var direction = section.getInt(team + ".direction");
                var materialList = section.getStringList(team + ".materials").stream().map(Material::valueOf).toList();
                teamBalls.add(new TeamBall(materialList, MAP_RADIUS.getValue() * signs[i][0], 64, signs[i][1] * MAP_RADIUS.getValue(), TEAM_ISLAND_RADIUS.getValue(), MIN_DISTANCE_BETWEEN_BALLS_AND_BORDER.getValue(), name, ChatColor.valueOf(color), direction));
            }
            i++;
        }
        return teamBalls;
    }


    private static List<CoreMaterial> readCores() {
        for(int i = 0 ; i < CoreMaterial.INIT_CORES.size() ; i++) {
            final String path = "generation.map.balls.materials.allowed." + i + ".";
            final CoreMaterial mat = CoreMaterial.INIT_CORES.get(i);
            BOSInit.CONFIG.addDefault(path + "material", mat.getCore());
            BOSInit.CONFIG.addDefault(path + "weight", mat.getWeight());
            BOSInit.CONFIG.addDefault(path + "forbiddenCrusts", mat.getForbiddenCrusts());
        }

        List<CoreMaterial> coreMaterials = new ArrayList<>();
        var section = BOSInit.CONFIG.getConfigurationSection("generation.map.balls.materials.allowed");
        for(var core : section.getKeys(false)) {
            var material = section.getString(core + ".material");
            var weight = section.getInt(core + ".weight");
            var forbiddenCrusts = section.getStringList(core + ".forbiddenCrusts");
            coreMaterials.add(new CoreMaterial(material, weight, forbiddenCrusts));
        }
        return coreMaterials;
    }

}
