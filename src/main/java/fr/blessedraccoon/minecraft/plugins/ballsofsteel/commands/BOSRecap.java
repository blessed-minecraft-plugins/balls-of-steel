package fr.blessedraccoon.minecraft.plugins.ballsofsteel.commands;

import fr.blessedraccoon.minecraft.plugins.commandmanager.CommandManager;
import fr.blessedraccoon.minecraft.plugins.commandmanager.RecapCommandValue;

public class BOSRecap extends RecapCommandValue {

    public BOSRecap(CommandManager manager) {
        super(manager);
    }

	@Override
	public String getPermission() {
		return "bos.recap";
	}
    
}
