package fr.blessedraccoon.minecraft.plugins.ballsofsteel.generation.randomness;

@FunctionalInterface
public interface Distribution {
    int getRandom();
}
