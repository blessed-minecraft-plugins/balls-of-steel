package fr.blessedraccoon.minecraft.plugins.ballsofsteel.generation.terrain;

import fr.blessedraccoon.minecraft.plugins.ballsofsteel.config.BOSConfig;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.generation.MapGeneration;

public class Borders {

    private int minDistance;

    public Borders(MapGeneration map) {
        this.minDistance = BOSConfig.MIN_DISTANCE_BETWEEN_BALLS_AND_BORDER.getValue();

        // TODO fix here
    }

    public int getMinDistance() {
        return minDistance;
    }

}