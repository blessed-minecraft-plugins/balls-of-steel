package fr.blessedraccoon.minecraft.plugins.ballsofsteel.generation.randomness;

public class UniformDistribution extends BornableDistribution {

    public UniformDistribution(int low, int high) {
        super(low, high);
    }

    @Override
    public int getRandom() {
        return (int) Math.floor(Math.random() * this.high);
    }
}
