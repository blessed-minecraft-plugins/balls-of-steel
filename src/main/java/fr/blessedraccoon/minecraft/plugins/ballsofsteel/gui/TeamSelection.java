package fr.blessedraccoon.minecraft.plugins.ballsofsteel.gui;

import fr.blessedraccoon.minecraft.plugins.ballsofsteel.BOSInit;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.config.BOSConfig;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.gameplay.BOSStart;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.gameplay.teams.TeamManager;
import net.kyori.adventure.text.Component;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scoreboard.Team;

public class TeamSelection implements Listener {

    @EventHandler(priority = EventPriority.HIGH)
    public void onJoin(PlayerJoinEvent e) {
        if (BOSStart.getGameState() == BOSStart.GameState.WAITING_TO_START) {
            var player = e.getPlayer();
            player.setGameMode(GameMode.ADVENTURE);
            player.setBedSpawnLocation(BOSInit.getOverworld().getSpawnLocation());
            player.getInventory().clear();

            for(var team : TeamManager.TEAMS) {
                ItemStack item = new ItemStack(team.getMaterialVariant(0));
                ItemMeta meta = item.getItemMeta();
                meta.displayName(Component.text(String.format("Rejoindre l'équipe %s%s%s (Clic Droit)", team.getColor(), team.getDisplayName(), ChatColor.GRAY)));
                item.setItemMeta(meta);

                player.getInventory().addItem(item);
            }
        }
    }

    @EventHandler
    public void rightClickWool(PlayerInteractEvent e) {
        if (BOSStart.getGameState() == BOSStart.GameState.WAITING_TO_START) {
            var player = e.getPlayer();
            var item = player.getInventory().getItemInMainHand();

            Material material = item.getType();

            for(var team : TeamManager.TEAMS) {
                if (team.getMaterialVariant(0) == material) {
                    this.handleJoin(team.getTeam(), team.getColor(), player);
                }
            }
        }

    }

    private void handleJoin(Team team, ChatColor color, Player player) {
        int onlinePlayers = Bukkit.getOnlinePlayers().size();
        int playersInTeam = team.getEntries().size();
        int nbTeam = TeamManager.TEAMS.length;

        int minPlayers = BOSConfig.GAME_MIN_PLAYERS.getValue();
        int maxPlayers = BOSConfig.GAME_MAX_PLAYERS.getValue();

        double theoreticalPlayersPerTeam = 0;
        if (onlinePlayers <= minPlayers) {
            theoreticalPlayersPerTeam = ((double) minPlayers) / nbTeam;
        } else {
            theoreticalPlayersPerTeam = ((double) Math.min(onlinePlayers, maxPlayers)) / nbTeam;
        }

        if (playersInTeam > theoreticalPlayersPerTeam) {
            player.sendMessage(Component.text(String.format("%sL'équipe %s%s%s est déjà complète !", ChatColor.GRAY, color, team.getName(), ChatColor.GRAY)));
        } else {
            team.addEntry(player.getName());
            player.sendMessage(Component.text(String.format("%sVous avez rejoint l'équipe %s%s%s !", ChatColor.GRAY, color, team.getName(), ChatColor.RESET)));
        }

        Component comp = Component.text(String.format("%s Elle est composée de :", ChatColor.GRAY));
        player.sendMessage(comp);
        for (var p : team.getEntries()) {
            comp = Component.text(String.format("- %s%s", color, p));
            player.sendMessage(comp);
        }
    }

}
