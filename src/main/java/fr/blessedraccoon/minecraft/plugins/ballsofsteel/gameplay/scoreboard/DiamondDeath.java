package fr.blessedraccoon.minecraft.plugins.ballsofsteel.gameplay.scoreboard;

import net.kyori.adventure.text.Component;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.scoreboard.Team;

import fr.blessedraccoon.minecraft.plugins.ballsofsteel.gameplay.BOSStart;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.gameplay.teams.TeamManager;

public class DiamondDeath implements Listener {
    

    @EventHandler
    public void onDeath(PlayerDeathEvent e) {
        if (BOSStart.getGameState() == BOSStart.GameState.RUNNING) {
            Player player = e.getEntity();
            var items = e.getDrops();
            var sum = items.stream()
                    .filter(i -> i.getType() == Material.DIAMOND)
                    .map(i -> i.getAmount())
                    .reduce(0, (acc, i) -> acc + i);
            if (sum > 0) {
                var team = TeamManager.getTeamBallOfPlayer(player);
                if (team != null) {
                    e.deathMessage(Component.text(String.format("%s est mort et fait perdre %s diamants à l'équipe %s !",
                            team.getColor() + player.getName() + ChatColor.RESET,
                            ChatColor.GOLD + "" + sum + ChatColor.RESET,
                            team.getColor() + team.getDisplayName() + ChatColor.RESET
                    )));
                }
            }
        }
    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent e) {
        if (BOSStart.getGameState() == BOSStart.GameState.RUNNING) {
            Player player = e.getPlayer();
            Team team = TeamManager.getTeamOfPlayer(player);
            PlayerInventory inv = player.getInventory();
            ItemStack[] armor = {
                new ItemStack(Material.LEATHER_HELMET),
                new ItemStack(Material.LEATHER_CHESTPLATE),
                new ItemStack(Material.LEATHER_LEGGINGS),
                new ItemStack(Material.LEATHER_BOOTS)
            };
            for (ItemStack piece : armor) {
                LeatherArmorMeta meta = (LeatherArmorMeta) piece.getItemMeta();
                Color color = null;
                switch(team.getColor()) {
                    case BLUE:
                        color = Color.BLUE;
                        break;
                    case RED:
                        color = Color.RED;
                        break;
                    case GREEN:
                        color = Color.GREEN;
                        break;
                    case YELLOW:
                        color = Color.YELLOW;
                        break;
                    default:
                        color = Color.WHITE;
                        break;
                }
                meta.setColor(color);
                piece.setItemMeta(meta);
            }
            inv.setHelmet(armor[0]);
            inv.setChestplate(armor[1]);
            inv.setLeggings(armor[2]);
            inv.setBoots(armor[3]);

            // --------------------------------------------

            inv.addItem(new ItemStack(Material.IRON_SWORD));
            inv.addItem(new ItemStack(Material.IRON_PICKAXE));
            ItemStack bow = new ItemStack(Material.BOW);
            ItemMeta metabow = bow.getItemMeta();
            metabow.addEnchant(Enchantment.ARROW_INFINITE, 1, true);
            bow.setItemMeta(metabow);
            inv.addItem(bow);
            inv.addItem(new ItemStack(Material.ARROW));
            inv.addItem(new ItemStack(Material.COOKED_BEEF, 8));
            inv.addItem(new ItemStack(Material.DIRT, 128));
            inv.addItem(new ItemStack(Material.TORCH, 32));
        }
    }
}
