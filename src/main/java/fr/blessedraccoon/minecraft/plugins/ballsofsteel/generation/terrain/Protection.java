package fr.blessedraccoon.minecraft.plugins.ballsofsteel.generation.terrain;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.extent.clipboard.Clipboard;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import org.bukkit.World;

public class Protection {

    private ProtectedRegion region;

    public Protection(ProtectedRegion reg) {
        this.region = reg;
    }

    public Protection(Clipboard clipboard, String name, int x, int y, int z) {
        var center = clipboard.getOrigin();

        BlockVector3 minimum = this.getCuboidSection(x, y, z, clipboard.getMinimumPoint(), center);
        BlockVector3 maximum = this.getCuboidSection(x, y, z, clipboard.getMaximumPoint(), center);

        this.region = new ProtectedCuboidRegion(name, minimum, maximum);
    }

    public void set(World world) {
        var regionsContainer = WorldGuard.getInstance().getPlatform().getRegionContainer();
        regionsContainer.get(BukkitAdapter.adapt(world)).addRegion(this.region);
    }

    private BlockVector3 getCuboidSection(int x, int y, int z, BlockVector3 point, BlockVector3 center) {
        return BlockVector3.at(
                x + point.getBlockX() - center.getBlockX(),
                y + point.getBlockY() - center.getBlockY(),
                z + point.getBlockZ() - center.getBlockZ()
        );
    }

    public ProtectedRegion getRegion() {
        return region;
    }
}
