package fr.blessedraccoon.minecraft.plugins.ballsofsteel.gameplay.scoreboard;

import net.kyori.adventure.text.Component;
import org.bukkit.Bukkit;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Scoreboard;

public class Health {

    public static final String HP = "PV";
    
    public Health() {
        Scoreboard scoreboard = Bukkit.getScoreboardManager().getMainScoreboard();
        scoreboard.registerNewObjective(HP, "health", Component.text(HP));
        scoreboard.getObjective("Health").setDisplaySlot(DisplaySlot.BELOW_NAME);
        scoreboard.getObjective("Health").setDisplaySlot(DisplaySlot.PLAYER_LIST);
    }

}
