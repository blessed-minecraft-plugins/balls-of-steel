package fr.blessedraccoon.minecraft.plugins.ballsofsteel.generation;

import com.sk89q.worldedit.extent.clipboard.Clipboard;
import com.sk89q.worldguard.protection.flags.Flags;
import com.sk89q.worldguard.protection.flags.StateFlag;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.BOSInit;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.config.BOSConfig;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.gameplay.BOSStart;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.gameplay.scoreboard.Health;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.gameplay.scoreboard.ScoreboardBuild;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.gameplay.teams.TeamManager;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.generation.terrain.BallGeneration;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.generation.terrain.Borders;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.generation.terrain.DiamondSign;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.generation.terrain.Protection;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;

import java.io.File;

public class MapGeneration {

    private int size;
    private int centerX;
    private int centerZ;

    private final Borders borders;
    private final BallGeneration ballGeneration;
    private TeamManager teamManager;

    private Protection center;

    public MapGeneration() {
        this.centerX = BOSConfig.MAP_CENTER_X.getValue();
        this.centerZ = BOSConfig.MAP_CENTER_Z.getValue();
        this.size = BOSConfig.MAP_RADIUS.getValue();
        this.borders = new Borders(this);
        this.ballGeneration = new BallGeneration(this);
    }


    public void doReplace(Location min, Location max, Material with) {
        for (int x = min.getBlockX(); x <= max.getBlockX(); x++) {
            for (int y = min.getBlockY(); y <= max.getBlockY(); y++) {
                for (int z = min.getBlockZ(); z <= max.getBlockZ(); z++) {
                    Block blk = min.getWorld().getBlockAt(new Location(min.getWorld(), x, y, z));
                    blk.setType(with);
                }
            }
        }
    }

    public void doReplace(Location min, Location max, Material replace, Material with) {
        for (int x = min.getBlockX(); x <= max.getBlockX(); x++) {
            for (int y = min.getBlockY(); y <= max.getBlockY(); y++) {
                for (int z = min.getBlockZ(); z <= max.getBlockZ(); z++) {
                    Block blk = min.getWorld().getBlockAt(new Location(min.getWorld(), x, y, z));
                    if (blk.getType() == replace) {
                        blk.setType(with);
                    }
                }
            }
        }
    }

    public void clearArea() {
        World world = BOSInit.getOverworld();

        Location loc1 = new Location(world, this.centerX - size + 1, 0, this.centerZ - size + 1);
        Location loc2 = new Location(world, this.centerX + size - 1, 180, this.centerZ + size - 1);

        doReplace(loc1, loc2, Material.AIR);
    }

    public void wall() {
        World world = BOSInit.getOverworld();

        doReplace(new Location(world, this.centerX - size, 0, this.centerZ - size), new Location(world, this.centerX - size, 180, this.centerZ + size), Material.BEDROCK);
        doReplace(new Location(world, this.centerX - size, 0, this.centerZ - size), new Location(world, this.centerX + size, 180, this.centerZ - size), Material.BEDROCK);
        doReplace(new Location(world, this.centerX + size, 0, this.centerZ - size), new Location(world, this.centerX + size, 180, this.centerZ + size), Material.BEDROCK);
        doReplace(new Location(world, this.centerX - size, 0, this.centerZ + size), new Location(world, this.centerX + size, 180, this.centerZ + size), Material.BEDROCK);
    }

    private void deleteScoreboardIfThere() {
        try {
            var sc = Bukkit.getScoreboardManager().getMainScoreboard();
            var scoreboard = sc.getObjective(ScoreboardBuild.NAME);
            scoreboard.unregister();
        } catch (IllegalArgumentException | IllegalStateException | NullPointerException ignored) {}
    }

    private void deleteHealthCounterIfThere() {
        try {
            var sc = Bukkit.getScoreboardManager().getMainScoreboard();
            var health = sc.getObjective(Health.HP);
            health.unregister();
        } catch (IllegalArgumentException | IllegalStateException | NullPointerException ignored) {}
    }

    private void deleteTeamsIfThere() {
        try {
            var sc = Bukkit.getScoreboardManager().getMainScoreboard();
            var teams = sc.getTeams();
            for(var team : teams) {
                team.unregister();
            }
        } catch (IllegalArgumentException | IllegalStateException ignored) {}
    }

    public void generate() {

        BOSStart.setGameState(BOSStart.GameState.WAITING_TO_START);

        this.deleteScoreboardIfThere();
        this.deleteHealthCounterIfThere();
        this.deleteTeamsIfThere();

        World world = BOSInit.getOverworld();

        this.clearArea();

        // --------------------------------------------------------------
        final String pathPrefix = "plugins/" + BOSInit.NAME + "/";
        File file = new File(pathPrefix + BOSConfig.MIDDLE_ISLAND_SCHEM_PATH.getValue());
        int radius = BOSConfig.MIDDLE_ISLAND_RADIUS.getValue();
        final int centerY = 80;
        final int centerX = 0;
        final int centerZ = 0;
        Clipboard clip = this.ballGeneration.load(centerX, centerY, centerZ, file, 0);
        Protection protection = new Protection(clip, "Centre", centerX, centerY, centerZ);
        var center = protection.getRegion();
        center.setFlag(Flags.BLOCK_BREAK, StateFlag.State.DENY);
        center.setFlag(Flags.EXIT, StateFlag.State.DENY);
        center.setFlag(Flags.FEED_AMOUNT, 20);
        center.setFlag(Flags.FEED_DELAY, 10);
        center.setFlag(Flags.PVP, StateFlag.State.DENY);
        protection.set(BOSInit.getOverworld());
        this.center = protection;
        this.ballGeneration.unConstruct(centerX, centerZ, radius);
        BOSInit.getOverworld().setSpawnLocation(centerX, centerY, centerZ);


        file = new File(pathPrefix + BOSConfig.TEAM_ISLAND_SCHEM_PATH.getValue());
        radius = BOSConfig.TEAM_ISLAND_RADIUS.getValue();

        for (int i = 0; i < BOSConfig.TEAMS.size(); i++) {
            int x = BOSConfig.TEAMS.get(i).getX();
            int y = BOSConfig.TEAMS.get(i).getY();
            int z = BOSConfig.TEAMS.get(i).getZ();
            clip = this.ballGeneration.load(x, y, z, file, BOSConfig.TEAMS.get(i).getDirection());
            protection = new Protection(clip, "Team_" + BOSConfig.TEAMS.get(i).getDisplayName(), x, y, z);
            var protectedTeamRegion = protection.getRegion();
            protectedTeamRegion.setFlag(Flags.BLOCK_BREAK, StateFlag.State.DENY);
            protection.set(BOSInit.getOverworld());

            Location loc1 = new Location(world, x - radius, y - radius, z - radius);
            Location loc2 = new Location(world, x + radius, y + radius, z + radius);

            this.doReplace(loc1, loc2, Material.WHITE_WOOL, BOSConfig.TEAMS.get(i).getMaterialVariant(0));
            this.doReplace(loc1, loc2, Material.YELLOW_WOOL, BOSConfig.TEAMS.get(i).getMaterialVariant(1));
            this.doReplace(loc1, loc2, Material.ORANGE_WOOL, BOSConfig.TEAMS.get(i).getMaterialVariant(2));
        }

        this.ballGeneration.unConstruct(Math.abs(BOSConfig.TEAMS.get(0).getX()), Math.abs(BOSConfig.TEAMS.get(0).getZ()), radius);

        // -------------------------------------------------------

        this.ballGeneration.generateBalls();
        this.wall();

        // -------------------------------------------------------

        this.teamManager = new TeamManager();
        this.teamManager.createTeams();
        var sc = ScoreboardBuild.getInstance();
        sc.build();
        sc.display();

        // -------------------------------------------------------

        DiamondSign.place(0, centerY, 0);

        BOSInit.getOverworld().getEntitiesByClass(Item.class).forEach(Entity::remove);

        BOSStart.setGameState(BOSStart.GameState.WAITING_TO_START);
    }

    public Borders getBorders() {
        return borders;
    }

    public TeamManager getTeamManager() {
        return this.teamManager;
    }

    public int getSize() {
        return size;
    }

    public int getCenterX() {
        return centerX;
    }

    public int getCenterZ() {
        return centerZ;
    }

    public void setCenterX(int centerX) {
        this.centerX = centerX;
    }

    public void setCenterZ(int centerZ) {
        this.centerZ = centerZ;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Protection getCenter() {
        return center;
    }
}
