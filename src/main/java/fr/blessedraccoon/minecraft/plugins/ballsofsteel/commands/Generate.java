package fr.blessedraccoon.minecraft.plugins.ballsofsteel.commands;

import org.bukkit.entity.Player;

import fr.blessedraccoon.minecraft.plugins.ballsofsteel.generation.MapGeneration;
import fr.blessedraccoon.minecraft.plugins.commandmanager.CommandPart;

public class Generate extends CommandPart {

    private MapGeneration mapgen;

    public Generate(MapGeneration mapgen) {
        this.mapgen = mapgen;
    }

    @Override
    public int getDeepLevel() {
        return 1;
    }

    @Override
    public String getDescription() {
        return "Generate the map";
    }

    @Override
    public String getName() {
        return "generate";
    }

    @Override
    public String getSyntax() {
        return "/bos generate";
    }

    @Override
    public boolean perform(Player player, String[] args) {
        this.mapgen.generate();
        return true;
    }

	@Override
	public String getPermission() {
		return "bos.generate";
	}
    
}
