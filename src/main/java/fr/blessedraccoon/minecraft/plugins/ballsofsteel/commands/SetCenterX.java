package fr.blessedraccoon.minecraft.plugins.ballsofsteel.commands;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import fr.blessedraccoon.minecraft.plugins.ballsofsteel.generation.MapGeneration;
import fr.blessedraccoon.minecraft.plugins.commandmanager.CommandPart;
import fr.blessedraccoon.minecraft.plugins.commandmanager.CommandValuable;

public class SetCenterX extends CommandPart implements CommandValuable {

    private MapGeneration mapgen;

    public SetCenterX(MapGeneration mapgen) {
        this.mapgen = mapgen;
    }

    @Override
    public String getValue() {
        return "" + this.mapgen.getCenterX();
    }

    @Override
    public int getDeepLevel() {
        return 1;
    }

    @Override
    public String getDescription() {
        return "Sets the center X value";
    }

    @Override
    public String getName() {
        return "setCenterX";
    }

    @Override
    public String getSyntax() {
        return "/bos setCenterX <Integer>";
    }

    @Override
    public boolean perform(Player player, String[] args) {
        try {
            int x = Integer.parseInt(args[this.getDeepLevel()]);
            this.mapgen.setCenterX(x);
            return true;
        } catch (NumberFormatException e) {
            player.sendMessage(ChatColor.RED + "The parameter you gave is not an Integer.");
            return false;
        }
    }

	@Override
	public String getPermission() {
		return "bos.setcenterx";
	}
    
}
