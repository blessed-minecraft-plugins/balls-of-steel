package fr.blessedraccoon.minecraft.plugins.ballsofsteel.gameplay.timer;

import fr.blessedraccoon.minecraft.plugins.ballsofsteel.config.BOSConfig;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.gameplay.BOSStart;
import fr.blessedraccoon.minecraft.plugins.timer.timing.TimeListener;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.TextColor;
import net.kyori.adventure.title.Title;
import org.bukkit.Bukkit;

import fr.blessedraccoon.minecraft.plugins.timer.timing.StandardTimer;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.time.Duration;

public class BOSEnding implements TimeListener {

    private int countdown;

    public BOSEnding() {
        this.countdown = BOSConfig.GAME_END_COUNTDOWN.getValue();
    }

    @Override
    public int getTriggerTime() {
        return this.countdown--;
    }

    @Override
    public void trigger(StandardTimer timer, long time) {
        if (this.countdown > 0) {
            Bukkit.getServer().audiences().forEach(p -> p.showTitle(Title.title(Component.text(this.countdown).color(TextColor.color(85, 255, 255)), Component.text(""), Title.Times.of(Duration.ofMillis(200), Duration.ofSeconds(700), Duration.ofMillis(100)))));
        } else {
            var title = new EndTitle();
            Bukkit.getServer().audiences().forEach(p -> p.showTitle(title));

            // Make players spectators
            BOSStart.setGameState(BOSStart.GameState.AFTER_END);
        }
    }

    static class EndTitle implements Title {

        @Override
        public @NotNull Component title() {
            return Component.text("La partie est terminée").color(TextColor.color(200, 0, 0));
        }

        @Override
        public @NotNull Component subtitle() {
            return Component.text("");
        }

        @Override
        public @Nullable Times times() {
            return Times.of(Duration.ofMillis(500), Duration.ofSeconds(10), Duration.ofMillis(500));
        }
    }


    
}
