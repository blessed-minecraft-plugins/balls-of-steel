package fr.blessedraccoon.minecraft.plugins.ballsofsteel;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.config.BOSConfig;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.gameplay.scoreboard.ScoreboardUpdater;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.gameplay.timer.BOSEnding;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.gui.TeamSelection;
import fr.blessedraccoon.minecraft.plugins.timer.timing.StandardTimer;
import fr.blessedraccoon.minecraft.plugins.timer.timing.TimerInvalidNameException;
import fr.blessedraccoon.minecraft.plugins.timer.timing.TimerManager;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import fr.blessedraccoon.minecraft.plugins.ballsofsteel.generation.terrain.DiamondSign;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.gameplay.BOSStart;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.gameplay.scoreboard.DiamondDeath;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.generation.MapGeneration;

public class BOSInit extends JavaPlugin {

    public static FileConfiguration CONFIG;
    public static final String NAME = "BlessedBOS";

    @Override
    public void onEnable() {

        CONFIG = this.getConfig();

        BOSConfig.saveDefaults(this);

        this.timerInit();

        MapGeneration mapGen = new MapGeneration();
        BOSStart start = new BOSStart(mapGen);

        // Event Listeners
        getServer().getPluginManager().registerEvents(new DiamondSign(), this);
        getServer().getPluginManager().registerEvents(new DiamondDeath(), this);
        getServer().getPluginManager().registerEvents(new TeamSelection(), this);
        getServer().getPluginManager().registerEvents(new ScoreboardUpdater(), this);

        var command = getCommand("bos");
        if (command != null) {
            command.setExecutor(new BOSManager(mapGen, start));
        }
        else {
            getLogger().severe("An error occurred while Balls of Steel plugin loading");
            return;
        }

        getLogger().info("The Balls of Steel plugin has been enabled");
    }



    private void timerInit() {
        try {
            TimerManager.getInstance().createTimer("BOS");
        } catch (TimerInvalidNameException e) {
            getLogger().severe("Problem during timer initialization. Shut down plugin.");
            return;
        }

        var timer = TimerManager.getInstance().getTimer("BOS");

        timer.setCountMethod(StandardTimer.COUNTDOWN_METHOD);
        timer.addTrigger(new BOSEnding());
    }

    public static WorldEditPlugin getWorldEdit() {
        return (WorldEditPlugin) Bukkit.getServer().getPluginManager().getPlugin("WorldEdit");
    }

    public static ProtectedRegion getRegion(String name) {
        var container = WorldGuard.getInstance().getPlatform().getRegionContainer();
        var regions = container.get(BukkitAdapter.adapt(getOverworld()));
        return regions.getRegion(name);
    }

    public static World getOverworld() {
        return Bukkit.getWorld(BOSConfig.OVERWORLD.getValue());
    }
}
