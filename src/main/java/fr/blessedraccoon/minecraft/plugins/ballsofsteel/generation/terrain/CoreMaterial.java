package fr.blessedraccoon.minecraft.plugins.ballsofsteel.generation.terrain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.blessedraccoon.minecraft.plugins.ballsofsteel.config.BOSConfig;

public class CoreMaterial {
    private String core;
    private int weight;
    private List<String> forbiddenCrusts;

    public CoreMaterial(String core, int weight, List<String> forbidden) {
        this.core = core;
        this.weight = weight;
        this.forbiddenCrusts = new ArrayList<>(Arrays.asList("water", "lava"));
        this.forbiddenCrusts.addAll(forbidden);
    }

    public CoreMaterial(String core, int weight) {
        this(core, weight, new ArrayList<>());
    }

    public static CoreMaterial draw() {
        final int sum = BOSConfig.CORE_MATERIALS.stream().map(CoreMaterial::getWeight).reduce((a, b) -> a + b).get();
        final int rand = (int) Math.floor(Math.random() * sum);
        int currSum = 0;
        CoreMaterial material = null;
        for (CoreMaterial coreMaterial : BOSConfig.CORE_MATERIALS) {
            currSum += coreMaterial.getWeight();
            if (currSum > rand) {
                material = coreMaterial;
                break;
            }
        }
        return material;
    }

    public String getCore() {
        return core;
    }

    public int getWeight() {
        return weight;
    }

    public List<String> getForbiddenCrusts() {
        return forbiddenCrusts;
    }

    public static final List<CoreMaterial> INIT_CORES = Arrays.asList(
            new CoreMaterial("iron_ore", 10),
            new CoreMaterial("coal_ore", 10),
            new CoreMaterial("water", 5, new ArrayList<>(List.of("cobweb"))),
            new CoreMaterial("lava", 5, new ArrayList<>(Arrays.asList("oak_log", "tnt", "cobweb"))),
            new CoreMaterial("oak_log", 15),
            new CoreMaterial("obsidian", 3),
            new CoreMaterial("tnt", 1, new ArrayList<>(List.of("redstone_block"))),
            new CoreMaterial("piston", 8),
            new CoreMaterial("glowstone", 10),
            new CoreMaterial("bricks", 10),
            new CoreMaterial("stone", 10),
            new CoreMaterial("lapis_ore", 5),
            new CoreMaterial("mycelium", 7),
            new CoreMaterial("glass", 3),
            new CoreMaterial("magma_block", 8),
            new CoreMaterial("redstone_block", 10, new ArrayList<>(Arrays.asList("tnt", "piston"))),
            new CoreMaterial("sponge", 3),
            new CoreMaterial("grass_block", 10),
            new CoreMaterial("purpur_block", 8),
            new CoreMaterial("nether_bricks", 8),
            new CoreMaterial("pumpkin", 6),
            new CoreMaterial("melon", 6),
            new CoreMaterial("packed_ice", 4),
            new CoreMaterial("sea_lantern", 8),
            new CoreMaterial("blue_glazed_terracotta", 6),
            new CoreMaterial("infested_stone", 4),
            new CoreMaterial("snow_block", 6),
            new CoreMaterial("cobweb", 2)
    );
}