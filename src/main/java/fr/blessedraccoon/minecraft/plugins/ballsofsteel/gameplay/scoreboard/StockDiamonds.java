package fr.blessedraccoon.minecraft.plugins.ballsofsteel.gameplay.scoreboard;

import net.kyori.adventure.text.Component;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import fr.blessedraccoon.minecraft.plugins.ballsofsteel.gameplay.teams.TeamManager;
import net.md_5.bungee.api.ChatColor;

public class StockDiamonds {

    private static final String NAME = "Diamonds";

    public static void stock(Player p, int count) {
        var team = TeamManager.getTeamBallOfPlayer(p);
        if (team != null && count > 0) {
            team.addScore(count);
            Bukkit.broadcast(Component.text(String.format("%s a déposé %s diamants pour l'équipe %s !",
                    team.getColor() + p.getName() + ChatColor.RESET,
                    ChatColor.GOLD + "" + count + ChatColor.RESET,
                    team.getColor() + team.getDisplayName(), ChatColor.RESET)));
        }
    }
    
}
