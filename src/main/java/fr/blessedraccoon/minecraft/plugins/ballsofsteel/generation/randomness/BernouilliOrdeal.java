package fr.blessedraccoon.minecraft.plugins.ballsofsteel.generation.randomness;

public class BernouilliOrdeal {
    
    private double p;

    public BernouilliOrdeal(double p) {
        this.p = (p >= 0 && p <= 1) ? p : 0.5;
    }

    public BernouilliOrdeal() {
        this(0.5);
    }

    public boolean run() {
        return Math.random() < this.p;
    }


}