package fr.blessedraccoon.minecraft.plugins.ballsofsteel.generation.terrain;

import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.function.pattern.RandomPattern;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.world.World;
import com.sk89q.worldedit.world.block.BlockState;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.BOSInit;
import org.bukkit.Material;

import java.util.Locale;

public class Ball {
    
    private final int radius;
    private final int diamondRate;

    private final String crust;
    private final String core;

    public Ball(String crust, String core, int radius, int diamondRate) {
        this.crust = crust;
        this.core = core;
        this.radius = radius;
        this.diamondRate = diamondRate;
    }

    public void create(int x, int y, int z) {

        World world = BukkitAdapter.adapt(BOSInit.getOverworld()); // Get the WorldEdit world from the spigot world by using BukkitAdapter
        try (var editSession = BOSInit.getWorldEdit().getWorldEdit().newEditSession(world)) { // get the edit session and use -1 for max blocks for no limit, this is a try with resources statement to ensure the edit session is closed after use
            // CORE
            var randomPattern = new RandomPattern(); // Create the random pattern


            BlockState coreState = BukkitAdapter.adapt(Material.valueOf(this.core.toUpperCase(Locale.ROOT)).createBlockData());
            BlockState diamond = BukkitAdapter.adapt(Material.DIAMOND_ORE.createBlockData());

            randomPattern.add(coreState, 100 - diamondRate);
            randomPattern.add(diamond, diamondRate);

            editSession.makeSphere(BlockVector3.at(x, y, z), randomPattern, this.radius - 1, true);

            // CRUST
            var crustPattern = new RandomPattern();

            BlockState crustState = BukkitAdapter.adapt(Material.valueOf(this.crust.toUpperCase(Locale.ROOT)).createBlockData());
            crustPattern.add(crustState, 100);

            editSession.makeSphere(BlockVector3.at(x, y, z), crustPattern, this.radius - 0.25, false);
        } catch (MaxChangedBlocksException ex) {
            ex.printStackTrace();
        }
    }

    public int getRadius() {
        return radius;
    }
}
