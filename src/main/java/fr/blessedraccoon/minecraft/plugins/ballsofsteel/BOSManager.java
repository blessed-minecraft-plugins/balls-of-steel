package fr.blessedraccoon.minecraft.plugins.ballsofsteel;

import org.bukkit.entity.Player;

import fr.blessedraccoon.minecraft.plugins.ballsofsteel.commands.*;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.gameplay.BOSStart;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.generation.MapGeneration;
import fr.blessedraccoon.minecraft.plugins.commandmanager.CommandManager;

public class BOSManager extends CommandManager {

    public BOSManager(MapGeneration mapGen, BOSStart start) {
        this.addSubcommand(new SetCenterX(mapGen));
        this.addSubcommand(new SetCenterZ(mapGen));
        this.addSubcommand(new SetMapRadius(mapGen));
        this.addSubcommand(new Generate(mapGen));
        this.addSubcommand(new StartCommand(start));
        // ------------------------
        this.addSubcommand(new BOSRecap(this));
        this.addSubcommand(new BOSHelp(this));
    }

    @Override
    public int getDeepLevel() {
        return 0;
    }

    @Override
    public String getDescription() {
        return "Main command, used to manage BoS";
    }

    @Override
    public String getName() {
        return "bos";
    }

    @Override
    public String getSyntax() {
        return "/bos";
    }

    @Override
	public boolean perform(Player arg0, String[] arg1) {
		return false;
    }

	@Override
	public String getPermission() {
		return "bos";
	}
}