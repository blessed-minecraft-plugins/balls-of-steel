package fr.blessedraccoon.minecraft.plugins.ballsofsteel.gameplay.teams;

import java.util.Locale;
import java.util.Set;

import fr.blessedraccoon.minecraft.plugins.ballsofsteel.BOSInit;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.config.BOSConfig;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Team;

import fr.blessedraccoon.minecraft.plugins.ballsofsteel.generation.terrain.TeamBall;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TeamManager {

    public static TeamBall[] TEAMS;
    
    public TeamManager() {
        TEAMS = BOSConfig.TEAMS.toArray(new TeamBall[0]);
    }

    public void createTeams() {
        var scoreboard = Bukkit.getScoreboardManager().getMainScoreboard();
        for (TeamBall team : TEAMS) {
            scoreboard.registerNewTeam(team.getDisplayName());
            var color = NamedTextColor.NAMES.value(team.getColorName().toLowerCase(Locale.ROOT));
            scoreboard.getTeam(team.getDisplayName()).color(color);
        }
    }

    public void setSpawns() {
        for (TeamBall teamBall : TEAMS) {
            this.setSpawn(teamBall.getTeam(), teamBall.getX(), teamBall.getY(), teamBall.getZ());
        }
    }

    public void setSpawn(@NotNull Team team, int x, int y, int z) {
        var entries = team.getEntries();
        for(var entry : entries) {
            var player = Bukkit.getPlayer(entry);
            if (player != null) {
                player.setBedSpawnLocation(new Location(BOSInit.getOverworld(), x, y, z), true);
            }
        }
    }

    @Nullable
    public static Team getTeamOfPlayer(@NotNull Player p) {
        Set<Team> teams = Bukkit.getScoreboardManager().getMainScoreboard().getTeams();
        Team team = null;
        for (Team curr : teams) {
            if (curr.hasEntry(p.getName())) {
                team = curr;
                break;
            }
        }
        return team;
    }

    @Nullable
    public static TeamBall getTeamBallOfPlayer(@NotNull Player p) {
        var team = getTeamOfPlayer(p);
        if (team == null) return null;
        for(TeamBall teamBall: TEAMS) {
            if (teamBall.getDisplayName().equals(team.getName())) {
                return teamBall;
            }
        }
        return null;
    }

}
