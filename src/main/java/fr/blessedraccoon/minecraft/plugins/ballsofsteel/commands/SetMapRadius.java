package fr.blessedraccoon.minecraft.plugins.ballsofsteel.commands;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import fr.blessedraccoon.minecraft.plugins.ballsofsteel.generation.MapGeneration;
import fr.blessedraccoon.minecraft.plugins.commandmanager.CommandPart;
import fr.blessedraccoon.minecraft.plugins.commandmanager.CommandValuable;

public class SetMapRadius extends CommandPart implements CommandValuable {

    private MapGeneration mapgen;

    public SetMapRadius(MapGeneration mapgen) {
        this.mapgen = mapgen;
    }

    @Override
    public String getValue() {
        return "" + this.mapgen.getSize();
    }

    @Override
    public int getDeepLevel() {
        return 1;
    }

    @Override
    public String getDescription() {
        return "Sets the radius of the map";
    }

    @Override
    public String getName() {
        return "setMapRadius";
    }

    @Override
    public String getSyntax() {
        return "/bos setMapRadius <Integer>";
    }

    @Override
    public boolean perform(Player player, String[] args) {
        try {
            int radius = Integer.parseInt(args[this.getDeepLevel()]);
            this.mapgen.setSize(radius);
            return true;
        } catch (NumberFormatException e) {
            player.sendMessage(ChatColor.RED + "The parameter you gave is not an Integer.");
            return false;
        }
    }

	@Override
	public String getPermission() {
		return "bos.setmapradius";
	}
    
}
