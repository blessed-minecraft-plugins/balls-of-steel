package fr.blessedraccoon.minecraft.plugins.ballsofsteel.commands;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import fr.blessedraccoon.minecraft.plugins.ballsofsteel.generation.MapGeneration;
import fr.blessedraccoon.minecraft.plugins.commandmanager.CommandPart;
import fr.blessedraccoon.minecraft.plugins.commandmanager.CommandValuable;

public class SetCenterZ extends CommandPart implements CommandValuable {

    private MapGeneration mapgen;

    public SetCenterZ(MapGeneration mapgen) {
        this.mapgen = mapgen;
    }

    @Override
    public String getValue() {
        return "" + this.mapgen.getCenterZ();
    }

    @Override
    public int getDeepLevel() {
        return 1;
    }

    @Override
    public String getDescription() {
        return "Sets the center Z value";
    }

    @Override
    public String getName() {
        return "setCenterZ";
    }

    @Override
    public String getSyntax() {
        return "/bos setCenterZ <Integer>";
    }

    @Override
    public boolean perform(Player player, String[] args) {
        try {
            int z = Integer.parseInt(args[this.getDeepLevel()]);
            this.mapgen.setCenterZ(z);
            return true;
        } catch (NumberFormatException e) {
            player.sendMessage(ChatColor.RED + "The parameter you gave is not an Integer.");
            return false;
        }
    }

	@Override
	public String getPermission() {
		return "bos.setcenterz";
	}
    
}
