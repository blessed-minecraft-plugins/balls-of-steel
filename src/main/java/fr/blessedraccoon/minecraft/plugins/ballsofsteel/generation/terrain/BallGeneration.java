package fr.blessedraccoon.minecraft.plugins.ballsofsteel.generation.terrain;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.WorldEditException;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.extent.clipboard.Clipboard;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardFormat;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardFormats;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardReader;
import com.sk89q.worldedit.function.operation.Operation;
import com.sk89q.worldedit.function.operation.Operations;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.math.Vector3;
import com.sk89q.worldedit.math.transform.AffineTransform;
import com.sk89q.worldedit.regions.CuboidRegion;
import com.sk89q.worldedit.regions.EllipsoidRegion;
import com.sk89q.worldedit.regions.Region;
import com.sk89q.worldedit.session.ClipboardHolder;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.config.BOSConfig;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.generation.randomness.PoissonDistribution;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.generation.randomness.UniformDistribution;

import fr.blessedraccoon.minecraft.plugins.ballsofsteel.BOSInit;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.generation.MapGeneration;
import fr.blessedraccoon.minecraft.plugins.ballsofsteel.generation.randomness.BornableDistribution;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;

public class BallGeneration implements Listener {

    private double sameCrustChance;
    private MapGeneration map;

    private int nbBalls;
    private int minimumBallDistance;
    private int minBallRadius;
    private int maxBallRadius;

    private BornableDistribution ballRadiusDistrib;
    private BornableDistribution diamondDistrib;
    private List<Region> unConstructibles;

    public BallGeneration(MapGeneration map) {
        this.sameCrustChance = BOSConfig.BALLS_SAME_CRUST_CHANCE.getValue();
        this.map = map;
        this.nbBalls = BOSConfig.MAX_BALL_NUMBER.getValue();
        this.minimumBallDistance = BOSConfig.MIN_DISTANCE_BETWEEN_BALLS.getValue();
        this.minBallRadius = BOSConfig.MIN_BALL_RADIUS.getValue();
        this.maxBallRadius = BOSConfig.MAX_BALL_RADIUS.getValue();
        final int lambda = BOSConfig.LAMBDA_BALL_RADIUS.getValue();
        this.ballRadiusDistrib = PoissonDistribution.getDistributionByConfig(this.minBallRadius, this.maxBallRadius, BOSConfig.BALL_RADIUS_DISTRIBUTION.getValue(), lambda);

        this.unConstructibles = new ArrayList<>();
        this.diamondDistrib = BornableDistribution.getDistributionByConfig(
                BOSConfig.MIN_DIAMONDS_PROPORTION.getValue(),
                BOSConfig.MAX_DIAMONDS_PROPORTION.getValue(),
                BOSConfig.DIAMONDS_PROPORTION_DISTRIBUTION.getValue(),
                BOSConfig.LAMBDA_DIAMONDS_PROPORTION.getValue());
    }

    public Clipboard load(int x, int y, int z, File file, int rotate) {
        Clipboard clipboard = null;

        ClipboardFormat format = ClipboardFormats.findByFile(file);
        try (ClipboardReader reader = format.getReader(new FileInputStream(file))) {
            clipboard = reader.read();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (EditSession editSession = WorldEdit.getInstance().getEditSessionFactory().getEditSession(new BukkitWorld(BOSInit.getOverworld()), -1)){
            ClipboardHolder clipboardHolder = new ClipboardHolder(clipboard);
            clipboardHolder.setTransform(new AffineTransform().rotateY(rotate));
            Operation operation = clipboardHolder
                .createPaste(editSession)
                .to(BlockVector3.at(x, y, z))
                .copyEntities(true)
                .ignoreAirBlocks(true)
                .build();


            Operations.completeLegacy(operation);

        } catch (WorldEditException e) {
            e.printStackTrace();
        }
        return clipboard;
    }

    public void unConstruct(int x, int z, int radius) {
        unConstructibles.add(new CuboidRegion(
                BlockVector3.at(x - radius, 0, z - radius),
                BlockVector3.at(x + radius, 256, z + radius))
        );
    }

    public void generateBalls() {
        final int timeOut = 500;

        for (int i = 0; i < nbBalls; i += 4) {

            EllipsoidRegion generatedRegion = null;
            boolean flag = true;
            int timeOutCount = 0;
            int radius = 0;
            int x = 0;
            int y = 0;
            int z = 0;
            while (flag && timeOutCount < timeOut) {
                flag = false;
                timeOutCount++;
                UniformDistribution distrib = new UniformDistribution(0, map.getSize() - BOSConfig.MIN_DISTANCE_BETWEEN_BALLS_AND_BORDER.getValue());
                x = (int) Math.floor(distrib.getRandom());
                y = (int) Math.floor(Math.random() * 128);
                z = (int) Math.floor(distrib.getRandom());
                radius = this.ballRadiusDistrib.getBornedRandom();

                var minDistanceBetweenBalls = BOSConfig.MIN_DISTANCE_BETWEEN_BALLS.getValue();
                generatedRegion = new EllipsoidRegion(
                        BlockVector3.at(x, y, z),
                        Vector3.at(radius + minDistanceBetweenBalls, radius + minDistanceBetweenBalls, radius + minDistanceBetweenBalls)
                );

                var center = generatedRegion.getCenter();
                BlockVector3[] points = {
                        BlockVector3.at(center.getX() + radius, center.getY(), center.getZ()),
                        BlockVector3.at(center.getX() - radius, center.getY(), center.getZ()),
                        BlockVector3.at(center.getX(), center.getY() + radius, center.getZ()),
                        BlockVector3.at(center.getX(), center.getY() - radius, center.getZ()),
                        BlockVector3.at(center.getX(), center.getY(), center.getZ() + radius),
                        BlockVector3.at(center.getX(), center.getY(), center.getZ() - radius),
                };

                for (Region region : unConstructibles) {

                    var minReg = region.getMinimumPoint();
                    var maxReg = region.getMaximumPoint();

                    for (var point : points) {
                        if (point.containedWithin(minReg, maxReg)) {
                            flag = true;
                            break;
                        }
                    }

                    if (flag) break;
                }
            }
            if (! flag) {
                this.generate(radius, this.diamondDistrib.getRandom(), x, y, z);
            }
        }
    }

    public void generate(int radius, int diamondRate, int x, int y, int z) {
        CoreMaterial core = CoreMaterial.draw();
        CoreMaterial crust = null;
        if (Math.random() > this.sameCrustChance || core.getForbiddenCrusts().contains(core.getCore())) {
            do {
                crust = CoreMaterial.draw();
            } while (core.getForbiddenCrusts().contains(crust.getCore()));
        }
        else {
            while (core.getForbiddenCrusts().contains(core.getCore())) {
                core = CoreMaterial.draw();
            }
        }
        if (crust == null) crust = core;
        this.generate(crust.getCore(), core.getCore(), radius, diamondRate, x, y, z);
    }

    public void generate(String crust, String core, int radius, int diamondRate, int x, int y, int z) {
        Ball b = new Ball(crust, core, radius, diamondRate);
        Bukkit.getLogger().info(x + " " + y + " " + z + " ---- " + radius);
        this.create(b, x, y, z);
        this.create(b, -x, y, -z);
        this.create(b, -z, y, x);
        this.create(b, z, y, -x);
    }

    public void create(Ball ball, int x, int y, int z) {
        this.unConstructibles.add(new EllipsoidRegion(
                BlockVector3.at(x, y, z),
                Vector3.at(ball.getRadius() + BOSConfig.MIN_DISTANCE_BETWEEN_BALLS.getValue(),
                        ball.getRadius() + BOSConfig.MIN_DISTANCE_BETWEEN_BALLS.getValue(),
                        ball.getRadius() + BOSConfig.MIN_DISTANCE_BETWEEN_BALLS.getValue())));
        ball.create(x, y, z);
    }
    
}
